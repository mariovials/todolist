# Todolist

Proyecto de postulación a GiSMA

---

Proyecto desarrollado usando [React 18.2](https://react.dev/) en el frontend y en el backend con una API RESTful con [Yii2](https://www.yiiframework.com/)

Demo: [http://18.222.21.121/](http://18.222.21.121/)

---

## Instalar aplicación

### Requerimientos (Debian 12.1)

- PostgreSQL (15)
- PHP 8.2+
- npm 9.8+
- Apache 2.4+

[Instalar requerimientos](/docs/instalar_requerimientos.md)

### Instalación

1. Clonar el repositorio desde [https://gitlab.com/mariovials/todolist.git](https://gitlab.com/mariovials/todolist)

```bash
git clone https://gitlab.com/mariovials/todolist.git todolist
```

### Configurar backend

#### Inicializar api

```bash
cd /full/path/todolist/src/api
composer install
chmod 777 runtime
```

#### Inicializar app (react)
```bash
cd /full/path/todolist/src/app
npm update
npm run build
```

#### Configurar web server (apache)

```bash
sudo ln -s /full/path/todolist/src/app/build/ /var/www/app
sudo ln -s /full/path/todolist/src/api/wev/ /var/www/api

sudo vi sudo vi /etc/apache2/sites-available/000-default.conf
<VirtualHost *:80>

  DocumentRoot "/var/www/app"

  <Directory "/var/www/app">
    AllowOverride All
  </Directory>

  Alias /api "/var/www/api"

  <Directory "/var/www/api">
    Header set Access-Control-Allow-Origin "*"
    RewriteEngine on
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule . index.php
  </Directory>

</VirtualHost>

sudo systemctl restart apache2
```
