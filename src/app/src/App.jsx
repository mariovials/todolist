import './app.css';
import TodoList from './components/TodoList';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CheckBoxIcon from '@mui/icons-material/CheckBox';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';

function App() {
  return (
    <Card  id="app" elevation={3}>
      <CardHeader 
        title="Lista de tareas" 
        avatar={
          <Avatar sx={{ bgcolor: '#1565c0' }}>
            <CheckBoxIcon />
          </Avatar>
        }>
      </CardHeader>
      <CardContent>
        <TodoList />
      </CardContent>
      <Typography id="hint">
      </Typography>
    </Card>
  )
}

export default App;
