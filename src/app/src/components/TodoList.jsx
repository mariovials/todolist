import React, { useEffect, useState } from "react"
import TodoInput from "./TodoInput"
import TodoItem from "./TodoItem"
import List from '@mui/material/List';
import * as api from "../api/TodoConnection"

const TodoList = () => {

  /**
   * props items array: [{
   *   id: integer,
   *   texto: string,
   *   completada: boolean
   * }, ...]
   */
  const [items, setItems] = useState([])

  /**
   * handlers
   */
  // agrega un elemento a la lista, lo guarda en db y lo muestra
  // muestra mensaje de tarea agregada
  const agregarItem = async (texto) => {
    return await api.add({
      texto,
      completada: false
    }).then((response) => {
      setItems([...items, response.data])
      document.getElementById('hint')
        .textContent = 'Tarea agregada';
    })
  }

  // quita un elemento de la lista y lo elimina en db
  const handleEliminarTarea = async (id) => {
    setItems(items.filter((item) => {
      return item.id !== id
    }))
    return await api.del(id)
  }

  // obtiene la lista desde db
  const getItems = async () => {
    setItems((await api.get()).data)
  }

  // cambia el valor de completada de una tarea según su id
  // se usa dentro de TodoItem
  const handleCompletada = (id) => {
    setItems(items.map(item => {
      if (item.id === id) {
        return {...item, completada: !item.completada}
      }
      return item
    }))
  }

  // obtiene items al iniciar la app
  useEffect(() => {
    getItems()
  }, [])

  return (
    <List id="todolist">
      {
        items.map((item, key) => {
          return (
            <TodoItem key={key}
              id={item.id}
              texto={item.texto}
              completada={item.completada}
              handleCompletada={handleCompletada}
              handleEliminarTarea={handleEliminarTarea} />
          )
        })
      }
      <TodoInput agregarItem={agregarItem} /> 
    </List>
  )
}
export default TodoList
