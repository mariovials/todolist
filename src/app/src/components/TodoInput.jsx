import React, { useEffect, useState } from 'react'
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AddIcon from '@mui/icons-material/Add';

const TodoInput = ({agregarItem}) => {

  // props texto
  const [texto, setTexto] = useState('')

  // handlers texto
  // cuando cambia el texto de la nueva tarea establece el texto
  // si el texto de la tarea no es vacio muestra el mensaje
  // 'Presione [Enter] para agregar la nueva tarea'
  // si el texto de la tarea es vacio limpia el mensaje
  const handleChange = (nuevo) => {
    setTexto(nuevo)
    if (nuevo.length) {
      document.getElementById('hint')
        .textContent = 'Presione [Enter] para agregar la nueva tarea';
    } else {
      document.getElementById('hint')
        .textContent = '';
    }
  }

  // maneja ingreso de tecla Shift+Tab, Fecha Arriba y Enter
  const handleKeyDown = (e) => {
    // si presiona Shift+Tab o Fecha Arriba
    // hace focus en tarea anterior
    if ((e.shiftKey && e.key === 'Tab') || e.key === 'ArrowUp') {
      e.preventDefault()
      e.target.closest('li').previousSibling.querySelector('.texto').focus()
      document.execCommand('selectAll', false, null)
      document.getSelection().collapseToEnd()
    }
    // si presiona Enter ingresa nueva tarea
    if (e.key.toLowerCase() === 'enter' && texto.trim().length) {
      // agrega clase agregando y pierde el foco en el input de nueva tarea
      e.target.closest('li').classList.add('agregando')
      e.target.blur()

      // muestra mensaje 'Agregando tarea...'
      document.getElementById('hint').textContent = 'Agregando tarea...';

      // agrega la tarea a la db
      // luego quita clase agregando, limpia el input de nueva tarea
      // y hace focus nuevamente en el input de nueva tarea
      agregarItem(texto).then(() => {
        setTexto('')
        e.target.focus()
        e.target.closest('li').classList.remove('agregando')
      })
    }
  }

  // cuando hace focus en el input de nueva tarea
  // agrega clase active para resaltar el item de nueva tarea
  const handleFocusInput = (e) => {
    e.target.closest('li').classList.add('active')
  }

  // cuando pierde el foco del input de nueva tarea
  // quita clase active del item
  const handleBlurInput = (e) => {
    e.target.closest('li').classList.remove('active')
  }

  // cuando hace click en el 'item de lista' de nueva tarea (no el input)
  // hace focus al final del input de nueva tarea
  const handleClickList = (e) => {
    if (e.target.id !== 'input') {
      document.getElementById('input').focus()
      document.execCommand('selectAll', false, null)
      document.getSelection().collapseToEnd()
    }
  }
  
  // pone el foco en el input de nueva tarea al inicio
  useEffect(() => {
    document.querySelector('#input').focus()
  }, [])

  return (

    <ListItem onClick={e => handleClickList(e)}>

      <ListItemIcon className='plus-icon'>
        <AddIcon/>
      </ListItemIcon>

      <ListItemText
        primary={ (
          <input
            autoComplete="off"
            id="input"
            type="text"
            value={texto}
            className="texto"
            placeholder='Nueva tarea'
            onChange={e => handleChange(e.target.value)}
            onKeyDown={e => handleKeyDown(e)}
            onFocus={e => handleFocusInput(e)}
            onBlur={e => handleBlurInput(e)}
          ></input>
        ) } />

      <ListItemIcon>
      </ListItemIcon>

    </ListItem>
  )
}
export default TodoInput
