import * as api from "../api/TodoConnection"
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

const TodoItem = (props) => {

  /**
   * item = {
   *   id: integer,
   *   texto: string,
   *   completada: boolean
   * }
   * 
   * handleEliminarTarea(id: integer)
   */

  // props
  const id = props.id
  const texto = props.texto
  const completada = props.completada

  // handlers

  // cuando cambia el checkbox de tarea completada
  // cambia el valor de completada en la lista mediante handleCompletada
  // envía el cambio a db y muestra el mensaje correspondiente
  const handleChangeCheckbox = () => {
    props.handleCompletada(id)
    api.edit(id, {
      completada: !completada
    })
    if (completada) {
      document.getElementById('hint').textContent = 'Tarea no completada'
    } else {
      document.getElementById('hint').textContent = 'Tarea completada'
    }
  }

  // maneja ingreso de Tab, Shift+Tab, Enter, Flecha arriba, Flecha abajo
  const handleKeyDown = (e) => {
    if (['Tab', 'Enter', 'ArrowDown', 'ArrowUp'].includes(e.key)) {
      e.preventDefault()
    }
    let li = null
    switch (e.key) {
      case 'Tab': case 'Enter': case 'ArrowDown':
        // pasa a siguiente item cuando presiona Tab, Enter, Flecha abajo
        li = e.target.closest('li').nextSibling
        // pasa a item anterior con Shift + tab
        if (e.shiftKey && e.key === 'Tab') {
          li = e.target.closest('li').previousSibling 
        }
      break;
      case 'ArrowUp': // pasa el foco a item anterior con flecha arriba
        li = e.target.closest('li').previousSibling 
      break;
      default: break;
    }
    // hace focus en el texto y mueve el cursor al final del texto
    if (li !== null) { 
      li.querySelector('.texto').focus()
      document.execCommand('selectAll', false, null)
      document.getSelection().collapseToEnd()
    }
  }

  // cuando se hace click en el boton eliminar tarea
  // borra el item de la lista, borra la terea de la db 
  // muestra mensaje Tarea eliminada
  // y mueve el cursor a la tarea siguiente
  const handleClickDelete = (e) => {
    props.handleEliminarTarea(id)
    e.target.closest('li').querySelector('.texto').focus()
    document.execCommand('selectAll', false, null)
    document.getSelection().collapseToEnd()
    document.getElementById('hint').textContent = 'Tarea eliminada';
  }

  // cuando hace focus en una tarea para editarla, destaca el item de la lista
  const handleFocusText = (e) => {
    e.target.closest('li').classList.add('active')
  }

  // cuando pierde el foco de una tarea existente
  // si se ha modificado su texto se envia el cambio a db
  // quita la clase active para no destacarla
  // muestra mensaje Tarea editada
  // // hacerlo cuando pierde el foco es más óptimo que hacerlo 
  // // cada vez que escribe una letra
  const handleBlurText = (e) => {
    e.target.closest('li').classList.remove('active')
    if (e.target.outerText !== texto) {
      api.edit(id, {
        texto: e.target.outerText,
      }).then(() => {
        document.getElementById('hint')
          .textContent = 'Tarea editada';
      })
    }
  }

  return (
    <ListItem disablePadding
      key={id}
      className={completada ? 'completada' : null}>

      <ListItemIcon>
        <Checkbox
          onChange={e => {handleChangeCheckbox(id)}}
          edge="start"
          checked={completada} />
      </ListItemIcon>

      <ListItemText
        className="texto"
        primary={texto}
        contentEditable={true} // para editar directamente la tarea
        suppressContentEditableWarning={true} // elimina el warning contentEditable 
        onKeyDown={e => {handleKeyDown(e)}}
        onFocus={e => {handleFocusText(e)}}
        onBlur={e => {handleBlurText(e)}}
        />

      <ListItemIcon className="btn-delete">
        <IconButton onClick={e => {handleClickDelete(e)}}>
          <DeleteIcon />
        </IconButton>
      </ListItemIcon>

    </ListItem>
  )
}
export default TodoItem
