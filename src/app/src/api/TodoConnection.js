import axios from "axios"

/**
 * Componente que centraliza el acceso a la API REST
 * Define una única vez la url
 * y provee los metodos que la aplicación necesita
 */

const url = 'http://18.222.21.121:8080/todo'

export const get = async function(params = []) {
  return await axios.get(url, params)
}
export const add = async function(params = []) {
  return await axios.post(url, params)
}
export const edit = async function(id, data = []) {
  return await axios.put(url + '/' + id, data)
}
export const del = async function(id) {
  return await axios.delete(url + '/' + id)
}
