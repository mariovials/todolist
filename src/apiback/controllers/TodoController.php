<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;

class TodoController extends ActiveController
{
    public $modelClass = 'app\models\Todo';

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'pagination' => false,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC,
                    ],
                ],
            ],
        ]);
    }
}
