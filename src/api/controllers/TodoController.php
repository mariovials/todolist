<?php

namespace app\controllers;

use yii\rest\ActiveController;
use yii\helpers\ArrayHelper;

/**
 * {@inheritdoc}
 * @author Mario Vial <mvial@ubiobio.cl> 2023/08/07 21:30
 */
class TodoController extends ActiveController
{
    public $modelClass = 'app\models\Todo';

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'pagination' => false,
                'sort' => [
                    'defaultOrder' => [
                        'id' => SORT_ASC,
                    ],
                ],
            ],
        ]);
    }
}
