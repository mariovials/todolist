<?php

namespace app\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;

/**
 * {@inheritdoc}
 * @author Mario Vial <mvial@ubiobio.cl> 2023/08/07 21:30
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
