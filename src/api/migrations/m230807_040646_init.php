<?php

use yii\db\Migration;

/**
 * Class m230807_040646_init
 */
class m230807_040646_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('todo', [
            'id' => $this->primaryKey(),
            'texto' => $this->string(),
            'completada' => $this->boolean()->notNull()->defaultValue(false),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('todo');
    }
}
