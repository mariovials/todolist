# Instalar requerimientos

1. Base
    
    ```bash
    sudo apt install curl zip
    ```
    
2. PHP (8.2+)
    
    ```bash
    sudo apt install php php-curl php-xml php-gd php-intl php-pgsql php-mbstring php-apcu php-memcache
    ```
    
3. PostgreSQL (12+)
    
    ```bash
    sudo apt install postgres
    sudo pg_ctlcluster 15 main start
    ```
    
    Configurar acceso de postgres
    
    ```bash
    sudo vi /etc/postgresql/15/main/postgresql.conf
    
    listen_addresses = '*'
    ```
    
    ```bash
    sudo vi /etc/postgresql/15/main/pg_hba.conf
    
    # TYPE  DATABASE        USER            ADDRESS                 METHOD
    local   all             postgres                                trust
    host    todolist        todolist_user   127.0.0.1/32            md5
    host    todolist        todolist_user   ::1/128                 md5
    ```
    
    ```bash
    sudo /etc/init.d/postgresql restart
    ```
    
    ```bash
    psql -U postgres
    CREATE database "todolist";
    
    CREATE USER "todolist_user" WITH PASSWORD 'l480!B0HK6w5';
    GRANT ALL PRIVILEGES ON DATABASE "todolist" to "todolist_user";
    
    \c todolist
    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "todolist_user";
    GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "todolist_user";
    ```
    
4. Apache 2
    
    ```bash
    sudo apt install apache2
    sudo a2enmod rewrite 
    sudo a2enmod headers
    ```
    

5. npm
    
    ```bash
    sudo apt install npm
    npm install -g n
    n latest
    npm install -g npm@latest
    ```